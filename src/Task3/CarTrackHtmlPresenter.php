<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $body = '';
        $footer = '';
        foreach ($track->all() as $car) {
            $body .= sprintf(
                $this->templateBody(),
                $car->getName(),
                $car->getImage()
            );

            $footer .= sprintf(
                $this->templateFooter(),
                $car->getName(),
                $car->getSpeed(),
                $car->getFuelTankVolume()
            );

        }

        return sprintf($this->templateHtml(), $body, $footer);
    }

    private function templateHtml()
    {
        return <<<PHP
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                %s
            </tbody>
        </table>
        <div><b>Fighters Info</b></div>
        <div>%s</div>    
        PHP;
    }

    private function templateBody()
    {
        return <<<PHP
        <tr>
            <td>%s</td>
            <td><img src="%s"></td>
        </tr>
        PHP;
    }

    private function templateFooter()
    {
        return '%s: %d, %d';
    }
}