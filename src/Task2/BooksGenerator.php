<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private $minPagesNumber;
    private $libraryBooks;
    private $maxPrice;
    private $storeBooks;

    public function __construct(int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBooks)
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate(): \Generator
    {
//        $minPagesNumber = $this->minPagesNumber;
//        yield from $this->filter(function($book) use ($minPagesNumber) {
//            return $book->getPagesNumber() >= $minPagesNumber;
//        }, $this->libraryBooks);
//
//        $maxPrice = $this->maxPrice;
//        yield from $this->filter(function($book) use ($maxPrice) {
//            return $book->getPrice() <= $maxPrice;
//        }, $this->storeBooks);

        yield from $this->getLibraryBooks();
        yield from $this->getStoreBooks();
    }

    private function getLibraryBooks(): iterable
    {
        foreach ($this->libraryBooks as $book) {
            if ($book->getPagesNumber() >= $this->minPagesNumber) {
                yield $book;
            }
        }
    }

    private function getStoreBooks(): iterable
    {
        foreach ($this->storeBooks as $book) {
            if ($book->getPrice() <= $this->maxPrice) {
                yield $book;
            }
        }
    }

    private function filter(callable $cb, $itr): iterable
    {
        foreach ($itr as $item) {
            $result = $cb($item);
            if ($result) {
                yield $item;
            }
        }
    }
}