<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    const SECOND_TO_HOUR = 3600;

    private float $lapLength;
    private int $lapsNumber;
    private array $cars = [];
    private float $totalLengthTrack;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ( $lapLength <= 0 || $lapsNumber <= 0) {
            throw new \InvalidArgumentException();
        }

        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;

        // общая длина которую должен проехать автомобиль
        $this->totalLengthTrack = $lapLength * $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        if ( empty($car) ) {
            throw new \InvalidArgumentException();
        }

        array_push($this->cars, $car);
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        if ( empty($this->cars) ) {
            throw new \InvalidArgumentException();
        }

        $result = [];

        foreach ($this->cars as $car) {

            $totalFuel = $car->getFuelsConsuptionForDistance($this->totalLengthTrack);
            // количество дозаправок
            $amountPitStop = $car->getNumberOfRefueling($totalFuel);
            // расчет времени на прохождение трассы с учетом дозаправок
            $totalTime = $this->convertSecondToHour($car->getPitStopTime($amountPitStop))
                + ($this->totalLengthTrack / $car->getSpeed());

            $result[$car->getId()] = $totalTime;
        }

        $max = array_keys($result, min($result));

        return $this->cars[array_key_first($max)];
    }

    private function convertSecondToHour(float $second): float
    {
        if ($second < 0) {
            throw new \InvalidArgumentException();
        }

        return $second / self::SECOND_TO_HOUR;
    }
}