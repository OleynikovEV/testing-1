<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    const HUNDRED_KILOMETERS = 100;

    private int $id;
    private string $image;
    private string $name;
    private int $speed;
    private int $pitStopTime; // second
    private float $fuelConsumption;
    private float $fuelTankVolume;

    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        if ( $speed <= 0 || $fuelConsumption <= 0 ||
            $fuelTankVolume <= 0 || $pitStopTime <= 0 ) {
            throw new \InvalidArgumentException();
        }

        $this->id = $id;
        $this->image = $image;
        $this->name = $name;
        $this->speed = $speed;
        $this->pitStopTime = $pitStopTime;
        $this->fuelConsumptio = $fuelConsumption;
        $this->fuelTankVolume = $fuelTankVolume;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(?float $amountPitStop = null): float
    {
        if ($amountPitStop != null) {
            return $amountPitStop * $this->pitStopTime;
        }

        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumptio;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }

    // расход топлива на расстояние км
    public function getFuelsConsuptionForDistance(float $length): float
    {
        if ( $length < 0 ) {
            throw new \InvalidArgumentException();
        }

        return ($length / self::HUNDRED_KILOMETERS * $this->getFuelConsumption());
    }

    // количество дозаправок
    public function getNumberOfRefueling(float $fuel_volume): float
    {
        if ( $fuel_volume < 0 ) {
            throw new \InvalidArgumentException();
        }
        // кол-во дозаправок -1 (машина начала движение уже с полным баком)
        return ceil(($fuel_volume / $this->getFuelTankVolume()) - 1);
    }
}